<?php

namespace App\Orchid\Layouts;

use App\Orchid\Filters\Articles\TitleFilter;
use Orchid\Filters\Filter;
use Orchid\Screen\Layouts\Selection;

class ArticleSelection extends Selection
{
    /**
     * @return Filter[]
     */
    public function filters(): iterable
    {
        return [
            TitleFilter::class
        ];
    }
}
