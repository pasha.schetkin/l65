<?php
namespace App\Orchid\Layouts;
use App\Models\Article;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ArticleListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'articles';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('id', 'ID')->sort(),
            TD::make('title', 'Title')
//                ->sort()
//                ->filter(TD::FILTER_TEXT)
                ->render(function (Article $article) {
                    return Link::make($article->title)
                        ->route('platform.articles.edit', $article);
                }),

            TD::make('created_at', 'Created')->sort()
                ->render(function (Article $article) {
                    return $article->created_at->diffForHumans();
                }),
            TD::make('updated_at', 'Last edit')->sort()->render(function (Article $article) {
                return '<strong style="color: red">' .
                    $article->updated_at->diffForHumans() .
                    '</strong>';
            }),
        ];
    }
}
