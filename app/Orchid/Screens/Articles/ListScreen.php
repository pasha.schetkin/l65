<?php

namespace App\Orchid\Screens\Articles;

use App\Models\Article;
use App\Orchid\Filters\Articles\TitleFilter;
use App\Orchid\Layouts\ArticleListLayout;
use App\Orchid\Layouts\ArticleSelection;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class ListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'articles' => Article::filters(ArticleSelection::class)->defaultSort('id')->paginate()
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Articles';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Create new')
                ->icon('bs.pencil')
                ->route('platform.articles.edit')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            ArticleSelection::class,
            ArticleListLayout::class
        ];
    }
}
