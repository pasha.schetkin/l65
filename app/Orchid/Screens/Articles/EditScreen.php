<?php
namespace App\Orchid\Screens\Articles;
use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;
class EditScreen extends Screen
{
    private Article $article;
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Article $article): iterable
    {
        $this->article = $article;
        return [
            'article' => $article
        ];
    }
    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        if (!$this->article->exists) {
            return 'Создание статьи';
        }
        return 'Редактирование статьи: ' . $this->article->title;
    }
    public function description(): ?string
    {
        return 'Создание/редактирование статьи';
    }
    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Save')
                ->icon('bs.check-circle')
                ->canSee(!$this->article->exists)
                ->method('store'),
            Button::make('Update')
                ->icon('bs.note')
                ->method('update')
                ->canSee($this->article->exists),
            Button::make('Remove')
                ->icon('bs.trash')
                ->method('remove')
                ->canSee($this->article->exists),
        ];
    }
    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('article.title')
                    ->title('Title')
                    ->placeholder('Attractive but mysterious title')
                    ->help('Specify a short descriptive title for this article.'),
                Relation::make('article.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),
                Quill::make('article.content')
                    ->title('Main text'),
                Upload::make('article.image'),
            ])
        ];
    }


    public function store(Request $request)
    {
        $validated = $request->validate([
            'article.title' => ['required', 'min:3', 'max:100'],
            'article.user_id' => ['required', 'exists:users,id'],
            'article.content' => ['required', 'min:5', 'max:2048']
        ], [
            'article.title.required' => 'Название статьи обязательное поле'
        ]);
        $this->article = new Article();
        $this->article->fill($request->get('article'))->save();

        $this->article->attachment()->syncWithoutDetaching(
            $request->input('article.image', [])
        );

        Alert::info('You have successfully created an article.');

        return redirect()->route('platform.articles.list');
    }

    public function update(Article $article, Request $request)
    {
        $article->fill($request->get('article'))->save();

        Alert::info('You have successfully created an article.');

        return redirect()->route('platform.articles.list');
    }

    /**
     * @param Article $article
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Article $article)
    {
        $article->delete()
            ? Alert::info('You have successfully deleted the article.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.articles.list');
    }
}
